# Proyecto Equipo 1


# Propuesta Proyecto Equipo 1

## 1. Objetivo

Desarrollar un proyecto sobre internet de las cosas (IoT) que sea elaborado por los métodos y
conocimientos adquiridos en la materia de Temas Selectos de Programación I.

## 2. Resumen

El proyecto es un sistema cuidador de mascotas. El sistema se basa en microservicios que nos
entregue información relevante sobre nuestra mascota, que es de interés cuando uno no está en
casa. El sistema está constituido por sensores conectados a un microcontrolador Argon Particle,
que a su vez enviará información mediante peticiones HTTP a un servidor; después, el servidor
registrará dicha información en una base de datos, mientras que muestra la información que el
usuario desee en una página web con interfaz amigable y fácil de usar.

## 3. Microservicios

## 3.1. Estado recursos de croquetas y agua

```
Se usará un dispensador para mascotas que almacenará tanto agua como croquetas. Se
implementarán sensores ultrasónico Hc-sr04 en la parte superior de ambos contenedores para
detectar la altura restante de los recursos (croquetas y agua) hasta agotarse. Esto con el fin de
saber cuándo se acabarán las croquetas para ser llenados de nuevo y evitar la preocupación de
no saber si se ya se acabaron o no.
```

## 3.2. Detección de mascota comiendo con dato de tiempo

```
En los platos donde serán puestos los alimentos y el agua, para que la mascota se alimente, se
pondrá un sensor de presencia Pir Hc-sr501 para detectar cuando la mascota se acerque al
plato a comer. Con esto sabremos cuanto tiempo estuvo comiendo la mascota.
```
## 3.3. Detección de temperatura y gas para situaciones de peligro

Cerca del dispensador se pondrán sensores de temperatura, humedad, y gas. El sensor de
temperatura y humedad es el Dht11, junto con este, para el de gas tenemos el MQ2.Esto con el fin
de saber la situación de la casa en cualquier momento y saber si nuestra mascota en peligro.


## 4. Servidor

Todos los microservicios anteriormente mencionados enviarán datos en tiempo real mediante
peticiones HTTP a un servidor desarrollado con nodejs y express. Este servidor será nuestro nexo
para conectar los datos etre la base de datos y la página web.

## 5. Base de datos

Una vez el servidor haya recibido los datos del Argon Particle, estos serán almacenados en una
base de datos sqlite3. Gracias a esto podremos tener acceso a la información en cualquier
momento que el usuario lo pida. Además de guardar las cuentas de registro de os usuarios.

## 6. Página web

La página web será desarrollada con el framework Angular, que nos ayudará a crear la página a
base de componentes y estilizarla Angular material y CSS. Tendremos un sistema de autenticación
al tener componentes de Login y Registro, que solicitan la veracidad de la información que
proporciona el usuario para tener acceso a la información.


