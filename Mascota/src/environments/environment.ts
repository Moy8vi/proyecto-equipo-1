// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBLlZ2NTOu0_5UDUJ3rWBG-LME3yKiaXgI",
  authDomain: "iot-pet-76a7c.firebaseapp.com",
  databaseURL: "https://iot-pet-76a7c.firebaseio.com",
  projectId: "iot-pet-76a7c",
  storageBucket: "iot-pet-76a7c.appspot.com",
  messagingSenderId: "372382080580",
  appId: "1:372382080580:web:62d3b11ea12ff84ec36611",
  measurementId: "G-XWGK1MNW8N"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
