import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {
  email: string= "";
  password: string="";
  nombre: string="";
  idParticle: string="";
  mascota: string="";


  constructor(private authenticationService: AuthenticationService, private router:Router) { }

  ngOnInit(): void {
  }

  Registrar(){
    this.authenticationService.register(this.email, this.password).then
    ((data)=>
    {this.authenticationService.login(this.email, this.password).then
      ((data1)=>
      {
        const user=
        {
          uid: data1.user.uid,
          email: this.email,
          name: this.nombre,
          idParticle: this.idParticle,
          mascota: this.mascota
        };
        this.authenticationService.createUser(user).then((data2)=>
        {
          console.log(data2);
        }).catch((error)=>
        {
          alert('Ocurrió un error');
          console.log(error);
        });
      }
    ).catch((error)=>
    {
      alert('Ocurrió un error');
      console.log(error);
    });
    alert('Registrado correctamente');
        this.router.navigate(['user']);
        console.log(data);
    }
    ).catch((error)=>
    {
      alert('Ocurrió un error');
      console.log(error);
    });
  }

}
